# git-branches

## Description
Artifact that communicates what I presented in my lightning talk presentation and addresses the same content, but provides more detail, while also being a way for the consumer to explore further.

## GitLab Pages
https://dan_willis.gitlab.io/git-branches/

## Author
Dan Willis